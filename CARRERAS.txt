CARRERA,CÓDIGO,NEM,RANK,LENG,MAT,HIST,CS,POND.,PSU,MAX.,MIN.,PSU,BEA,FACULTAD
Arquitectura,19020,10,40,20,20,10,-,500,475,728.80,569.90,120,3,FACULTAD DE ARQUITECTURA
Cine 19024,10,40,30,10 ,10,10,500,475,762.40,572.90,40,5,FACULTAD DE ARQUITECTURA
Diseño,19022,10,40,20,20,10,10,500,475,692.10,504.60,75,4,FACULTAD DE ARQUITECTURA
Gestión en Turismo y Cultura,19027,10,40,30,10,10,10,500,475,721.70,549.10,40,5,FACULTAD DE ARQUITECTURA
Teatro (requiere prueba especial),19028,10,20,40,10,20,-,500,475,762.76,574.92,20,5,FACULTAD DE ARQUITECTURA
Ingeniería en Estadística,19072,10,40,10,30,10,ó,10,500,475,671.80,506.80,30,5,FACULTAD DE CIENCIAS
Lic. en Ciencias mención: Biología o Química,19075,10,40,20,20,-,10,500,475,735.50,538.10,25,2,FACULTAD DE CIENCIAS
Lic. en Física mención: Astronomía, Ciencias Atmosféricas o Computación Científica,19078,10,20,15,45,-,10,500,500,659.65,510.00,45,5,FACULTAD DE CIENCIAS
Licenciatura en Matemáticas,19070,10,40,10,30,10,10,500,475,632.20,506.10,15,2,FACULTAD DE CIENCIAS
Biología Marina,19080,10,40,15,25,-,10,500,475,691.35,502.45,50,5,FACULTAD DE CIENCIAS DEL MAR Y DE RECURSOS NATURALES
Administración Hotelera y Gastronómica,19086,10,40,20,20,10,10,500,475,731.60,500.80,60,5,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Administración Pública - Valparaíso,19068,10,20,35,25,10,-,500,500,723.45,574.90,75,2,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Administración Pública - Santiago,19005,10,20,35,25,10,-,500,500,655.60,571.45,75,2,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Auditoría - Valparaíso,19062,10,40,20,15,15,15,500,475,725.45,509.65,100,5,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Auditoría - Santiago,19093,10,40,20,15,15,15,500,475,729.65,500.55,60,5,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Auditoría Vespertino - Valparaíso,19069,10,40,20,15,15,15,500,475,700.15,504.55,20,0,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Ingeniería Comercial - Viña del Mar,19060,10,40,10,30,10,-,500,500,705.20,578.10,150,12,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Ingeniería Comercial - Santiago,19095,10,40,10,30,10,-,500,500,647.30,500.90,125,12,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Ingeniería en Información y Control de Gestión,19083,10,40,10,30,10,10,500,475,722.80,501.80,50,5,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Ingeniería en Negocios Internacionales - Viña,19088,10,30,20,30,10,10,500,475,693.60,529.20,80,5,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Ingeniería en Negocios Internacionales - Stgo.,19089,10,30,20,30,10,10,500,475,694.70,511.80,35,5,FACULTAD DE CIENCIAS ECONÓMICAS Y ADMINISTRATIVAS
Derecho,19030,10,25,20,20,25,-,500,500,737.30,628.90,135,5,FACULTAD DE DERECHO Y CIENCIAS SOCIALES
Trabajo Social,19031,20,30,20,20,10,-,500,475,700.00,556.40,78,2,FACULTAD DE DERECHO Y CIENCIAS SOCIALES
Química y Farmacia,19090,10,40,20,20,-,10,500,500,760.80,620.50,60,2,FACULTAD DE FARMACIA
Nutrición y Dietética,19091,10,40,20,20,-,10,500,500,770.40,608.90,60,2,FACULTAD DE FARMACIA
Pedagogía en Filosofía,19010,10,40,20,10,20,-,500,500,718.90,505.70,30,2,FACULTAD DE HUMANIDADES
Pedagogía en Historia y Ciencias Sociales,19011,10,40,20,10,20,-,500,500,741.90,552.60,70,3,FACULTAD DE HUMANIDADES
Pedagogía en Música,19015,10,40,25,15,10,-,500,500,619.60,529.95,40,2,FACULTAD DE HUMANIDADES
Sociología,19012,10,40,20,15,15,-,500,475,736.35,546.05,75,2,FACULTAD DE HUMANIDADES
Ingeniería Ambiental,19074,10,30,10,40,-,10,500,500,740.70,547.70,55,4,FACULTAD DE INGENIERÍA
Ingeniería Civil,19021,10,20,15,45,10,10,500,500,659.10,523.05,90,3,FACULTAD DE INGENIERÍA
Ingeniería Civil Ambiental,19018,10,20,10,45,-,15,500,500,S/I,Carrera nueva,35,2,FACULTAD DE INGENIERÍA
Ingeniería Civil Biomédica,19076,10,30,10,40,10,10,500,500,749.80,584.00,60,2,FACULTAD DE INGENIERÍA
Ingeniería Civil Industrial - Valparaíso,19052,10,30,10,40,10,10,500,500,690.70,582.20,110,10,FACULTAD DE INGENIERÍA
Ingeniería Civil Industrial - Santiago,19094,10,30,10,40,10,10,500,500,691.00,563.10,90,10,FACULTAD DE INGENIERÍA
Ingeniería Civil Informática,19085,10,30,10,40,10,10,500,500,651.70,541.60,75,5,FACULTAD DE INGENIERÍA
Ingeniería Civil Matemática,19019,10,20,10,45,-,15,500,500,S/I,Carrera nueva,15,2,FACULTAD DE INGENIERÍA
Ingeniería Civil Oceánica,19081,10,40,10,30,10,10,500,475,626.60,501.20,30,2,FACULTAD DE INGENIERÍA
Ingeniería en Construcción,19026,10,25,10,45,10,10,500,475,674.55,544.10,80,5,FACULTAD DE INGENIERÍA
Educación Parvularia,19037,10,40,30,10,10,10,500,500,652.10,530.20,35,2,FACULTAD DE MEDICINA
Enfermería - Reñaca,19041,10,40,25,15,-,10,500,500,773.10,681.40,75,3,FACULTAD DE MEDICINA
Enfermería - San Felipe,19042,10,40,25,15,-,10,500,500,729.85,645.80,40,2,FACULTAD DE MEDICINA
Fonoaudiología - Reñaca,19046,10,40,30,10,-,10,500,500,729.70,633.70,45,2,FACULTAD DE MEDICINA
Fonoaudiología - San Felipe,19044,10,40,30,10,-,10,500,500,664.60,566.10,40,2,FACULTAD DE MEDICINA
Kinesiología,19043,10,30,20,20,-,20,500,500,745.95,602.60,80,2,FACULTAD DE MEDICINA
Medicina - Reñaca,19040,10,30,20,20,-,20,600,600,815.90,762.50,72,2,FACULTAD DE MEDICINA
Medicina - San Felipe,19039,10,30,20,20,-,20,600,600,764.10,751.30,42,2,FACULTAD DE MEDICINA
Obstetricia y Puericultura - Reñaca,19047,10,40,30,10,-,10,500,500,778.10,691.10,75,2,FACULTAD DE MEDICINA
Obstetricia y Puericultura - San Felipe,19036,10,40,30,10,-,10,500,500,723.50,670.20,25,2,FACULTAD DE MEDICINA
Psicología,19045,10,30,30,20,10,-,500,500,700.20,623.80,80,3,FACULTAD DE MEDICINA
Tecnología Médica - Reñaca,19049,10,40,25,15,-,10,500,500,756.00,697.20,55,2,FACULTAD DE MEDICINA
Tecnología Médica - San Felipe,19048,10,40,25,15,-,10,500,500,819.15,649.10,40,2,FACULTAD DE MEDICINA
Odontología,19050,10,40,20,20,-,10,500,500,762.10,690.50,72,2,FACULTAD DE ODONTOLOGÍA